package app.fedilab.android.animatemenu.interfaces;

import app.fedilab.android.fragments.ContentSettingsFragment;

/**
 * Created by Konstantin on 12.01.2015.
 */
public interface Resourceble {
    public int getImageRes();

    public ContentSettingsFragment.type getType();
}
